const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const PUBLIC = __dirname + "/dist"
const TITLE = "[name]"


const plugins = [
    // environment variables
    new webpack.DefinePlugin({
        __ENV__: JSON.stringify(process.env.NODE_ENV)
    }),
    new webpack.ProvidePlugin({
        THREE: 'three',
        // ...
    }),
    new HtmlWebpackPlugin({
        title: TITLE,
        filename: 'index.html',
        template: 'src/index.html',
        chunks: []
    }),
    // create an html page
    new HtmlWebpackPlugin({
        title: TITLE,
        filename: 'FBO-Example.html',
        template: 'src/index.html',
        chunks: ['fbo-example']
    }),	// create an html page
    new HtmlWebpackPlugin({
        title: TITLE,
        filename: 'Animation-Example.html',
        template: 'src/index.html',
        chunks: ['animation-example']
    }),
    //copy the assets (with no css compilation)
    new CopyWebpackPlugin([
        {from: 'src/assets', to: 'assets'},
        {from: 'src/css', to: 'css'}
    ]),
    // clean the output folder
    new CleanWebpackPlugin(['dist']),
]

module.exports = {
    target: 'web',
    devtool: 'source-map',
    entry: {
        "fbo-example": './src/js/fbo-example-app.js',
        "animation-example": './src/js/animation-example-app.js',
        "animation-library": './src/js/animation-library.js'
    },
    output: {
        filename: 'js/[name].js',
        path: PUBLIC,
    },
    plugins: plugins,
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, 'src/js')
                ],
                loader: 'eslint-loader'
            },
            {
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, 'src/js')
                ],
                loader: 'babel-loader',
                query: {
                    compact: true,
                    presets: [
                        ['env', {modules: false}]
                    ]
                }
            },
            {
                test: /\.(glsl|frag|vert)$/,
                use: [
                    'raw-loader',
                    {
                        loader: 'glslify-loader', options: {}
                    }
                ],
                include: [path.resolve(__dirname, 'src/js')]

            }
        ]
    },
    performance: {
        hints: false
    }
}
