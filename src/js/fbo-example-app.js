import Config from './app/data/config'
import Main from './app/main'
import FBOSampleScene from "./app/FBOSampleScene";

if (__ENV__ === 'dev') {
    Config.isDev = true
}

function init() {
    new FBOSampleScene()
}

init()
