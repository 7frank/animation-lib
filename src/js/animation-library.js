/**
 * place stuff that will be exported here
 *
 *
 */


import {Expression} from "./math/Expression";
import {ExpressionPointCloudHelper, pointCloudMorph} from "./helper/ExpressionPointCloudHelper";
import {createGraph} from "./helper/ExpressionGraphHelper";
import {derivativeHelper} from "./helper/ExpressionDerivativeHelper";
import {curve2line, curve2line2} from "./helper/CurveHelper";
import {PathAnimation} from "./animation/PathAnimation";
import {Animation} from "./animation/Animation";
import {createFBOMorph} from "./fbo/fbo_test";

export const object = {   
    math: {
        Expression
    },
    helper: {
        ExpressionPointCloudHelper,
        pointCloudMorph,
        createGraph,
        derivativeHelper,
        curve2line,
        curve2line2
    },
    animation: {
        PathAnimation,
        Animation,
        createFBOMorph
    }


}