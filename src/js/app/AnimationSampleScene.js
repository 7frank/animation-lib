import * as THREE from "three";

import Main from "./main";
import Cube from "./blocks/cube";
import {curve2line, curve2line2} from "../helper/CurveHelper";
import {Expression} from "../math/Expression";
import {PathAnimation} from "../animation/PathAnimation";
import TWEEN from "@tweenjs/tween.js/src/Tween";
import {ExpressionPointCloudHelper, pointCloudMorph} from "../helper/ExpressionPointCloudHelper";
import {derivativeHelper} from "../helper/ExpressionDerivativeHelper";
import {createGraph} from "../helper/ExpressionGraphHelper";



/**
 * FIXME bad ordering of axis
 * @deprecated
 * @param target
 * @returns {PathAnimation}
 */
export function createShakeHelper(target) {
    let expr = new Expression("f(x,y)=sin(x)/x")

    let curvePoints = [
        new THREE.Vector2(0, 0),
        new THREE.Vector2(10, 0),


    ]


    let myCurve = expr.getCurve(curvePoints, true)

    let anim = new PathAnimation(new THREE.Vector3(), myCurve, {
        easing: TWEEN.Easing.Quadratic.InOut,
        yoyo: true,
        repeat: 0
    })


    let target0 = target.clone()

    anim.animate(1, 0.001, 1000)
    anim.on('update', function (vec) {
        let vec0 = new THREE.Vector3(vec.x, vec.z, vec.y)
        if (isNaN(vec.z)) vec.z = 0

        target.copy(target0.clone().add(vec0))
        //derivative.updatePosition(x,y)
        console.log(vec, target)
        //   derivative.updateFu(mesh, x, y)
        //   light.position.copy(mesh.position)
        //   light.position.y++

    })


    return anim

}


export default class AnimationSampleScene extends Main {

    createScene()
    {


        let container = new THREE.Object3D()

        let cube = new Cube(1, 1, 1)
        container.add(cube.mesh)

        this.scene.add(container)
        container.rotation.set(-Math.PI / 2, 0, -Math.PI / 2)

        this.testAnimation(container, this.camera, this.renderer, cube.mesh)




    }


    /**
     *
     * TODO evaluate possible changes and use cases
     */
     testAnimation(scene, camera, renderer, mesh) {


        let curvePoints = [
            new THREE.Vector2(-8, -8),
            new THREE.Vector2(-8, 8),
            new THREE.Vector2(8, 8),
            new THREE.Vector2(8, -8)

        ]


        let curve = new THREE.CatmullRomCurve3(curvePoints, true)


        let targetParams = {
            x: {min: -10, max: 10, step: 0.05},
            y: {min: -10, max: 10, step: 0.05},
        };
        let func = '2(sin(x)+cos(y))'
        let funcccc = 'f(x, y) = ' + func
        let func2 = "0.03(x^2*sin(x)+y^2*cos(y))+20"
        //let func2 = "5"
        // let func2 = "x^2"
        let funcccc2 = 'f(x, y) = ' + func2


        let line = curve2line(curve)
        scene.add(line)

        let line2 = curve2line2(curve, func, 300)
        scene.add(line2)


//--------------
        let expr = new Expression(funcccc)

        let myCurve = expr.getCurve(curvePoints, true)

        let secondPathAnimation = new PathAnimation(mesh.position, myCurve, {
            easing: TWEEN.Easing.Quadratic.InOut,
            yoyo: false,
            repeat: Infinity
        })

        secondPathAnimation.animate(0, 1, 10000)

//----------------------


        let step = 0.5

        let funcPointCloud = ExpressionPointCloudHelper(func2, step)


        scene.add(funcPointCloud)

        //invisible - only there to have points for morph target
        let interpolationPointCloud = ExpressionPointCloudHelper(func, step)


        pointCloudMorph(funcPointCloud, interpolationPointCloud, 5000, {
            easing: TWEEN.Easing.Quadratic.InOut,
            yoyo: true,
            repeat: 0
        })


        let derivative = derivativeHelper(func)

        scene.add(derivative)

//-------------------------

        var light = new THREE.PointLight(0xffFFFF);

        scene.add(light);

        var sphereSize = 1;
        var pointLightHelper = new THREE.PointLightHelper(light, sphereSize);
        scene.add(pointLightHelper);


        var geometry = new THREE.PlaneGeometry(20, 20);
        var material = new THREE.MeshStandardMaterial({
            color: 0xaaaaaa,
            side: THREE.DoubleSide,
            transparent: true,
            opacity: 0.5
        });
        var plane = new THREE.Mesh(geometry, material);
        plane.position.set(0, 0, -5)
        scene.add(plane);
//-------------------------


        secondPathAnimation.on('update', function ({x, y}) {

            //derivative.updatePosition(x,y)

            derivative.updateFu(mesh, x, y)
            light.position.copy(mesh.position)
            light.position.y++

        })


        let graphMesh = createGraph(func)
        scene.add(graphMesh);


        let graphMesh2 = createGraph(func2)
        scene.add(graphMesh2);


//-------------------


    }




}
