import * as THREE from "three";
import {createSampleFBOs} from "./fbo.example";
import Main from "./main";


export default class FBOSampleScene extends Main {

    createScene()
    {

        //this.camera.position.set(0,0,-20)

        let container = new THREE.Object3D()
        this.scene.add(container)
        container.rotation.set(-Math.PI / 2, 0, -Math.PI / 2)

        //testAnimation(container, this.camera, this.renderer, cube.mesh)

        let elements=createSampleFBOs(container, this.camera, this.renderer,this.controls)


        window.addEventListener("click", ()=>elements.next());

        /*window.addEventListener("wheel", event => console.info(event.wheelDelta), false);
        window.addEventListener("mousewheel", clickHandler, false);
        // Firefox
        window.addEventListener("DOMMouseScroll", clickHandler, false);
        */




    }



}