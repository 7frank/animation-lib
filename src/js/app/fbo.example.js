import {Expression} from "../math/Expression";
import * as THREE from "three";
import * as _ from "lodash";
import {createFBOMorph, createFBONoise} from "../fbo/fbo_test";
import {Animation} from "../animation/Animation";
import {getSphereDataTexture} from "../fbo/texture-utils";



export
function easeOutElastic(t) {
    if (t == 1) return 1
    var p = 0.3;
    return Math.pow(2, -10 * t) * Math.sin((t - p / 4) * (2 * Math.PI) / p) + 1;
}

/**
 *
 * @param datatexture - a data texture that holds the values that shall be manipulated
 * @param randMaxArray - currently the size of the randMaxArray determines how the function writes dataTexture. e.g. [10,0,0,0] will randomise the value of the first and every fourth entry in the dataTexture.
 */
export function addNoiseToDataTexture(datatexture, randMaxArray) {
    let data = datatexture.image.data

    let len = data.length
    let step = randMaxArray.length

    for (let i = 0; i < len; i += step) {

        for (let j = 0; j < step; j ++)
        data[i+j] += Math.random() * randMaxArray[j]


    }
    datatexture.needsUpdate = true;
}

export function createSampleFBOs(scene, camera, renderer, controls) {








    let funcccc2 = 'f(x, y) = 5'
    //expressions to fbo morph

    let expr2 = new Expression(funcccc2)
    let width = 256, height = 256


    let expr3 = new Expression('f(x, y) = (x^2+y^2)*0.4+10')
    //let randomDataTexture=getRandomDataTexture({x:0,y:0,z:20},width / 4, height / 4, 20)
    let sphereDataTexture=getSphereDataTexture(width , height , 15)
    let randomDataTexture = expr3.getDataTexture()


    addNoiseToDataTexture(randomDataTexture, [1, 1, 10])

    //TODO fallback texture with error information if loading fails

    let firstColorTexture = THREE.ImageUtils.loadTexture("assets/textures/bigbuckbunny.jpg")
    let minionTexture = THREE.ImageUtils.loadTexture("assets/textures/minions.jpg")
    let secondColorTexture = THREE.ImageUtils.loadTexture("assets/textures/mario.jpg")
    let particle = THREE.ImageUtils.loadTexture("assets/particles/dot7.png") // "assets/particles/particle_mask.jpg"

    let planeTexture = expr2.getDataTexture()





    //testing noise simulation --------------------------------
    let fboNoise = createFBONoise(renderer, {source:sphereDataTexture,sourceColor:firstColorTexture}, {pointSize: 50}, {width,height})
    // fbo1.fbo.particles.position.y = 30
    fboNoise.fbo.particles.scale.multiplyScalar(1)
    fboNoise.fbo.particles.visible = true
    scene.add(fboNoise.fbo.particles);
    //entries.unshift(fboNoise)
    //testing noise simulation --------------------------------








    function FBOFactory(name, options, shaderOptions, miscOptions) {
        let defaults = {source: planeTexture, target: randomDataTexture}
        options = _.assignIn(defaults, options)

        return function () {

            let fbo1 = createFBOMorph(renderer, options, shaderOptions, miscOptions)
           // fbo1.fbo.particles.position.y = 30
            fbo1.fbo.particles.scale.multiplyScalar(1)


            fbo1.fbo.particles.visible = false


            scene.add(fbo1.fbo.particles);
            return fbo1


        }

    }


    //TODO have a list of 200 images from remote to test performance
    // only keep a subset in RAM
    let list = [
        FBOFactory("entry 1", {sourceColor: firstColorTexture,particle}, {pointSize:50,transparent: true}, {width, height}, {
            width,
            height
        }),
        FBOFactory("missing entry", {}, {pointSize:50}, {width, height}, {
            width,
            height
        }),
        FBOFactory("entry 2", {sourceColor: minionTexture}, {pointSize:50}, {width, height}),
        FBOFactory("entry 3", {sourceColor: secondColorTexture}, {pointSize:50}, {width, height}),
        FBOFactory("entry 4", {sourceColor: "https://i.imgur.com/phwePtR.jpg"}, {pointSize:50}, {width, height}),
        FBOFactory("entry 5", {sourceColor: "https://i.imgur.com/4Q3uEQ1.jpg"}, {pointSize:50}, {width, height}),
        FBOFactory("entry 6", {sourceColor: "https://i.imgur.com/hTsvCVK.jpg"}, {pointSize:50}, {width, height}),
        FBOFactory("entry 7", {sourceColor: "https://i.imgur.com/WEssWkT.jpg"}, {pointSize:50}, {width, height}),
        FBOFactory("entry 8", {sourceColor: "https://i.imgur.com/9AgnRX0.jpg"}, {pointSize:25}, {width:width*2, height:height*2}),


    ]


    let animation, animation2;





    let menuPos = 0;
    let entries = list.map((v) => v())


    //FIXME implement caching strategy... currently all images are loaded in advance because ..
    //TODO  define how many images are loaded in advance instead
    //let entries = Array(3).fill(false)


    //entries.forEach((fbo) => fbo.fbo.particles.visible = false)

    entries[0]=list[0]()
    entries[0].fbo.particles.visible = true



    function updateHelper(fbo) {

        return function () {
            //update params
            fbo.simulationShader.uniforms.timer.value = this.position;
           // fbo.renderShader.uniforms.pointSize.value = 50

        }
    }


    function updateTextureOfMorphFBO(fbo,otherTexture){

        //TODO use noise texture for morph in separate example
      fbo.simulationShader.uniforms.textureB.value=otherTexture

    }


    function clickHandler(menuPos) {




        // createShakeHelper(controls.target)


        let currentEntry, nextEntry
        if (animation) animation.stop()
        if (animation2) animation2.stop()

        let p1=menuPos % entries.length
        let p2=(menuPos + 1) % entries.length
        // create and update next
        currentEntry = entries[p1]?entries[p1]:entries[p1]=list[p1]()
        nextEntry = entries[p2]?entries[p2]:entries[p2]=list[p2]()


        //This will set the noise texture dynamically for the morph target parameter overriding other morph targets
        updateTextureOfMorphFBO(nextEntry,fboNoise.fbo.getCurrentSimulationTexture())



        currentEntry.fbo.particles.visible = true
        animation = new Animation({position: 0}, {
            easing: (t) => (--t) * t * t + 1 /*easeOutElastic*/, repeat: 0, yoyo: true
        });

        animation2 = new Animation({position: 1}, {
            easing: (t) => (--t) * t * t + 1 /*easeOutElastic*/, repeat: 0, yoyo: true
        });

        animation.animate({position: 1}, 1000, () => {
            currentEntry.fbo.particles.visible = false

            nextEntry.fbo.particles.visible = true
            animation2.animate({position: 0}, 200, () => {


            }, updateHelper(nextEntry))


        }, updateHelper(currentEntry))


    }


    return {
        goto:function(i=0){

            clickHandler(i)


        },
        next:function(){

            clickHandler(menuPos)
            menuPos++

        },
        prev:function(){
            clickHandler(menuPos)
            menuPos--

        }


    }

}