import {BoxGeometry, MeshBasicMaterial, Mesh} from 'three'

export default class Cube {
    constructor(x = 1, y = 1, z = 1) {
        let geometry = new BoxGeometry(x, y, z)
        let material = new MeshBasicMaterial({color: 0x00ff00})

        this.mesh = new Mesh(geometry, material)
    }

    update() {
        this.mesh.rotation.x += 1
        this.mesh.rotation.y += 0.1
    }
}
