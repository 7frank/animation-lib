import * as THREE from 'three'

import Stats from 'stats.js'

import Config from './data/config'

import Cube from './blocks/cube'

import '../animation/PathAnimation'

import {createControls} from "./controls.example";
import {FPSCtrl} from "../animation/FPSCtrl";


export default class Main {
    constructor() {

        this.clock = new THREE.Clock();

        this.scene = new THREE.Scene()
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000)
        this.renderer = new THREE.WebGLRenderer({antialias: true})

        this.renderer.setSize(window.innerWidth, window.innerHeight)

        document.body.style.margin = 0
        document.body.appendChild(this.renderer.domElement)

        this.camera.position.z = 20
        this.camera.position.y = -20
        this.camera.position.x = -20



        this.addLights()
        this.addHelpers()

        window.addEventListener('resize', () => {
            let width = window.innerWidth
            let height = window.innerHeight

            this.renderer.setSize(width, height)

            this.camera.aspect = width / height
            this.camera.updateProjectionMatrix()
        })

        this.createControls()
        this.createScene()

        this.addStats()

        // let's render

        this.renderLoop=new FPSCtrl(60,function(){
            this.render()
        },this).start()



    }

    createControls()
    {
        //TODO improve code for controls
        this.controls = createControls(this.camera, this.scene)

    }

    createScene() {
        let cube = new Cube(1, 1, 1)
        this.scene.add(cube.mesh)



    }

    render() {
        if (Config.isDev) {
            this.stats.begin()
        }


        var delta = this.clock.getDelta();

        if (this.controls.update)
            this.controls.update(delta)

        this.renderer.render(this.scene, this.camera)

        if (Config.isDev) {
            this.stats.end()
        }


    }

    addLights() {
        let ambientLight = new THREE.AmbientLight(0x000000)
        this.scene.add(ambientLight)

        let light = new THREE.PointLight(0xffffff, 1, 0)
        light.position.set(0, 200, 0)
        this.scene.add(light)
    }

    addHelpers() {
        if (Config.isDev) {
            let axisHelper = new THREE.AxisHelper(50)
            this.scene.add(axisHelper)

            var size = 10;
            var divisions = 10;

            // var gridHelper = new THREE.GridHelper( size, divisions );
            // this.scene.add( gridHelper );


        }
    }

    addStats() {
        if (Config.isDev) {
            this.stats = new Stats()
            this.stats.showPanel(0) // 0: fps, 1: ms, 2: mb, 3+: custom
            document.body.appendChild(this.stats.domElement)
        }
    }
}


