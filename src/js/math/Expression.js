import * as THREE from "three";
import math from "mathjs";

/**
 * see where that leads us
 * @param expression
 */
export class Expression {

    constructor(expression) {
        this.mExpression = expression
        this.mParser = math.parser()
        this.mParser.eval(expression)

        //this.variables = Object.getOwnPropertyNames(targetParams)
        /*    this.variables.reduce(function(result, item, index, array) {
                result[item] = 0; //a, b, c
                return result;
            }, {})
            */

    }

    /**
     * Based on the points passed a math object will be generated that uses the expression to evaluate a z-coordinate
     * @param points
     * @param closed
     */
    getCurve(points, closed) {
        var that = this;
        let curve = new THREE.CatmullRomCurve3(points, closed)


        var fn = curve.getPoint

        curve.getPoint = function (t, optionalTarget) {

            let {x, y} = fn.apply(curve, arguments)
            let str = Object.values({x, y}).join(",")
            var val = that.mParser.eval(`f(${str})`);

            if (optionalTarget)
                Object.assign(optionalTarget, {x, y, z: val})

            return optionalTarget ? optionalTarget : new THREE.Vector3(x, y, val)

        }

        return curve

    }

    getMesh() {


    }


    _getData(xmin = -10, xmax = 10, width = 256, ymin = -10, ymax = 10, height = 256) {

        //TODO refactor
        const node = math.parse(this.mExpression)
        const code = node.compile()


        let xstep = (xmax - xmin) / width
        let ystep = (ymax - ymin) / height

        var data = new Float32Array(height * width * 3);

        var i = 0;
        for (var x = xmin; x < 10; x += xstep) {
            for (var y = -10; y < 10; y += ystep) {

                //FIXME returns NaN
                //let val = code.eval({x, y})

                let str = Object.values({x, y}).join(",")
                var val = this.mParser.eval(`f(${str})`);


                data[i] = x
                data[i + 1] = y;
                data[i + 2] = val;

                i += 3

            }
        }

        return data;
    }

    getDataTexture(height = 256, width = 256) {


        var dataA = this._getData(-10, 10, width, -10, 10, height)

        var textureA = new THREE.DataTexture(dataA, width, height, THREE.RGBFormat, THREE.FloatType, THREE.DEFAULT_MAPPING, THREE.RepeatWrapping, THREE.RepeatWrapping);
        textureA.needsUpdate = true;


        return textureA

    }


    /**
     * Will iterate through the geometry vertices and alter the z attribute by running the expression parser.
     * The result is a 3d representation of the expression.
     *
     * TODO mapping - implement  mapping  ,mapping={x:"x",y:"y"}
     * @param {THREE.Geometry} geometry
     *
     */
    toGeometry(geometry) {

        geometry.vertices.forEach(v => {
            v.z = this.mParser.eval(`f(${v.x},${v.y})`);
        })

    }

    /**
     * Will generate a g
     * @param width
     * @param height
     * @param widthSegments
     * @param heightSegments
     * @param {THREE.Vector3} position
     * @returns {PlaneGeometry}
     */

    toPlaneGeometry(width, height, widthSegments, heightSegments) {
        let graphGeometry = new THREE.PlaneGeometry(width, height, widthSegments, heightSegments)


        this.toGeometry(graphGeometry)
        return graphGeometry

    }


}