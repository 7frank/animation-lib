import Config from './app/data/config'

import AnimationSampleScene from "./app/AnimationSampleScene";

if (__ENV__ === 'dev') {
    Config.isDev = true
}

function init() {
    new AnimationSampleScene()
}

init()
