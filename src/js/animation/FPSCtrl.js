import {Clock} from 'three'
import * as EventEmitter from 'eventemitter3';

/**
 * @callback onFrameCallback
 * @param {object} info - An object containing additional information of the current frame.
 * @param {number} info.time - A timestamp.
 * @param {number} info.frame - The auto incrementation identifier of the current frame.
 * @param {FPSCtrl} info.script - A reference to the FPSCtrl itself.
 *
 */


/**
 * A simple FPS - limiter.
 * For reference to the original code see {@link https://stackoverflow.com/a/19773537}
 * Implements some additional performance measurement to keep track of frames that are really slow.
 *
 *  TODO refactor into class and extend EE
 *
 * @param {number} fps - A Number indicating the times per second 'onFrame' gets called.
 * @param {onFrameCallback} onFrame - A callback function.
 * @param {object} [context] - A context the 'onFrame' functions gets bound to.
 * @constructor
 */
export function FPSCtrl(fps = 30, onFrame, context) {
    var delay = 1000 / fps, // calc. time per frame
        time = null, // start time
        frame = -1, // frame count
        tref; // rAF time reference

    this.mFunction=onFrame
    this.mMaxFPS=fps
    this._events=new EventEmitter();


    this.mTimer=new Clock(false);

    var that = this;

    if (context != null) {
        onFrame = onFrame.bind(context);
    }

    this.mContext = context;

    function scriptLoop(timestamp) {
        if (time === null) time = timestamp; // init start time
        var seg = Math.floor((timestamp - time) / delay); // calc frame no.
        if (seg > frame) { // moved to next frame?
            frame = seg; // update

            that.mTimer.start();

            onFrame({ // callback function
                time: timestamp,
                frame: frame,
                script: that
            });

            that._events.emit("frame")


        }
        tref = requestAnimationFrame(scriptLoop);
    }

    // play status
    this.isPlaying = false;

    // set frame-rate
    this.frameRate = function (newfps) {
        if (!arguments.length) return fps;
        fps = newfps;
        delay = 1000 / fps;
        frame = -1;
        time = null;

        return this;
    };

    this.getCurrentFPS = function () {
        return fps;
    };

    // enable starting/pausing of the object
    this.start = function () {
        if (!this.isPlaying) {
            this.isPlaying = true;
            tref = requestAnimationFrame(scriptLoop);
        }
        return this;
    };

    // circumvent fps limit
    // alter 'time' so the next time loop gets called the next frame will be updated
    this.forceNext = function () {
        time -= delay;
        return this;
    };

    this.pause = function () {
        if (this.isPlaying) {
            cancelAnimationFrame(tref);
            this.isPlaying = false;
            time = null;
            frame = -1;
        }
        return this;
    };

    this.stop = function () {
        return this.pause();
    };


    this.on=function(event,cb)
    {
        this._events.on(event,cb)

        return this
    }


}
