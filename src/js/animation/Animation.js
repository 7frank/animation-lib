import TWEEN from '@tweenjs/tween.js';
import * as _ from "lodash"
import {FPSCtrl} from "./FPSCtrl";
import {flatten} from "../helper/points-utils";

import * as EventEmitter from 'eventemitter3';


//TODO refactor
TWEEN.Tween.prototype.from=function (object) {
    this._object = object;
    return this
}

/**
 * Wrapper around tween-js with some additional functionality.
 *
 * TODO deep copy array entries
 */
export class Animation extends EventEmitter {

    /**
     *
     * @param {Object|Object[]} mSource - The source object or array which gets mutated.
     * @param options
     * @param {function} options.easing - An easing function used for the interpolation between source and target.
     * @param {number} options.repeat -How many times the animation is repeated. Valid values range between 0  and Infinity.
     * @param {boolean} options.yoyo - Whether the repetition shall restart rom the source value or if it should instead bounce back.
     */
    constructor(mSource, options = {}) {

        super()




        if (typeof mSource === 'undefined') throw new Error('must be an object or array of some sort');

        this.mObject = mSource
        this.mSource = mSource




        this.mOptions = _.assignIn({easing: (v) => v, repeat: 0, yoyo: false}, options)

        this.mTween = new TWEEN.Tween();

        // tail for chaining
        this.mTail= this.mTween




    }


    /**
     *
     * @param {Object|Object[]} mTarget - The source object or array that are used to mutate 'this.mSource'.
     * @param mDuration
     * @param onComplete
     * @param onUpdate
     * @returns {TWEEN.Tween} - exposes tween for further manipulation {@link https://github.com/tweenjs/tween.js/blob/master/docs/user_guide.md}
     */
    animate(mTarget = {}, mDuration = 400, onComplete, onUpdate) {


        var self = this
        if (this.mTween) this.mTween.stop();

        let tStart = performance.now()
        let tStop = tStart + mDuration


        var that = this.mSource;
        var stopped = false;

        var flattened_to = flatten(mTarget);

        var keys = Object.keys(flattened_to);
        var flattened_from = {};
        keys.forEach(k => flattened_from[k] = _.get(that, k));


        // whenever animation is completed or stopped
        const onFinish = () => {
            if (stopped) return //in case it was already stopped either from complete or finish callback

            // stop animation

            this.mScript.stop()


            stopped = true;

            if (typeof onComplete === 'function') {

                onComplete.bind(this.mObject)();
            }
            self.emit("complete",self.mObject)
        }





        this.mTween.from(flattened_from).to(flattened_to, mDuration)
            .easing(this.mOptions.easing)
            .repeat(this.mOptions.repeat)
            .yoyo(this.mOptions.yoyo)
            .onUpdate(function (values) {
                let tCurrent = performance.now()

                // Move the values from the flattened and tweening object
                // to the original object

                for (let key in values) {
                    _.set(that, key, values[key])
                }


                if (typeof onUpdate === 'function'){

                    onUpdate.bind(that)({
                        current: tCurrent - tStart,
                        max: tStop - tStart
                    });}
                self.emit("update",self.mObject)

            })
            .onComplete(onFinish).onStop(onFinish)
        //.start();


        this.mScript = new FPSCtrl(60, frame => {
            if (stopped) return;

            this.mTween.update(frame.time);

        })
        //.start()

        this.start()

        return this.mTween;
    }

    stop() {
        this.mTween.stop();
        this.mScript.stop();
        return this;
    }


    start() {
        this.mTween.start();
        this.mScript.start();
        return this;
    }

    /**
     * FIXME
     * @param obj
     */
    pushAnimation(obj,onUpdate)
    {


        let anim=new Animation(this.mSource,this.mOptions)

        anim.animate(obj,5000,()=>{},onUpdate)//.stop()
        anim.stop()

        this.mTail.chain(anim.mTween);

        return this

    }

    /**
     * TODO use as last argument to loop through animation
     */
    loopChain()
    {
        this.mTail.chain(this.mTween);
        return this
    }

}
