/**
 *
 * TODO heightmap from function .... f(x,y) ... heightmap then use Func
 */


/**
 * abstraction layer for path animation
 * create it as stand alone module to be able to use it as part of ai package for vr sandbox as well
 *
 */
import {Animation} from "./Animation";


//function path interpolation

//


/**
 *
 *
 * example:
 * let expr=new Expression("x^2+y^2")
 * let myCurve=expr.getCurve(curvePoints,true)
 * let pathAnim = new PathAnimation(mesh.position,  myCurve,  {easing:TWEEN.Easing.Quadratic.InOut,yoyo:false,repeat:Infinity})
 * pathAnim.animate(0,1,10000)
 */


export class PathAnimation extends Animation {

    /**
     *
     * @param {THREE.Vector3} mObject
     * @param {THREE.Curve} curve
     * @param options
     */
    constructor(mObject, curve, options = {}) {

        super({}, options)
        this.mSource = {position: 0}


        this.mCurve = curve
        this.mObject = mObject
    }


    /**
     * TODO use start stop or remove params
     * @param {number} start
     * @param {number} stop
     * @param duration
     */
    animate(start, stop, duration = 1000) {
        var that = this



        return super.animate({position: 1}, duration, () => {

        }, function update() {

            that.mCurve.getPoint(this.position, that.mObject)


        })


    }

}






