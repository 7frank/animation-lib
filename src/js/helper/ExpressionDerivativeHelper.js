import * as THREE from "three";
import math from "mathjs";

/**
 * Helper to render the derivative of a 3d function like f(x,y)
 *
 * @param expression
 * @returns {Object3D}
 */
export function derivativeHelper(expression) {

    var geometry = new THREE.PlaneGeometry(1, 1);
    var material = new THREE.MeshBasicMaterial({color: 0xffff00, side: THREE.DoubleSide});
    var plane = new THREE.Mesh(geometry, material);

    var obj = new THREE.Object3D();
    obj.add(plane)

    const f = math.parse(expression).compile()
    const dfx = math.derivative(expression, 'x').compile()
    const dfy = math.derivative(expression, 'y').compile()


    obj.updatePosition = function (x, y) {
        let dx = dfx.eval({x, y})
        let dy = dfy.eval({x, y})
        let fix = Math.atan(dx)
        let fiy = Math.atan(dy)


        var euler = new THREE.Euler(fix, fiy, 0, 'XYZ');
        obj.quaternion.setFromEuler(euler)
        //console.log(dx,dy,fix,fiy)


        //----------------------------

        let val = f.eval({x, y})
        obj.position.set(x, y, val)


    }

    obj.updateFu = function (target, x, y) {
        let dx = dfx.eval({x, y})
        let dy = dfy.eval({x, y})
        let fix = Math.atan(dx)
        let fiy = Math.atan(dy)

//FIXME result not as intended
        var euler = new THREE.Euler(fix, fiy, 0, 'XYZ');
        target.quaternion.setFromEuler(euler)
        //console.log(dx,dy,fix,fiy)


        //----------------------------


    }


    return obj
}