import * as THREE from "three";
import * as Gradient from "tinygradient";
import {getFunc} from "./ExpressionPointCloudHelper";
import {Expression} from "../math/Expression";


function createGradient(size) {

    var colors = Gradient(['white', 'green', 'blue'])


    return colors
}


/**
 * Can be used in conjunction with ParametricGeometry to generate geometries based on mathematical expressions.
 *
 * @param {string} expression - An expression in the form f(x,y)=x+y
 * @param xMin
 * @param xMax
 * @param yMin
 * @param yMax
 * @returns {meshFunction}
 * @constructor
 */
function ParametricExpressionFunctionFactory(expression, xMin = -10, xMax = 10, yMin = -10, yMax = 10) {

    var zFunc = getFunc(expression)

    var xRange = xMax - xMin,
        yRange = yMax - yMin;


    //Parser.parse(zFuncText).toJSFunction( ['x','y'] );
    var meshFunction = function (x, y) {
        x = xRange * x + xMin;
        y = yRange * y + yMin;
        var z = zFunc(x, y); //= Math.cos(x) * Math.sqrt(y);
        if (isNaN(z))
            return new THREE.Vector3(0, 0, 0); // TODO: better fix
        else
            return new THREE.Vector3(x, y, z);
    };


    return meshFunction

}

/**
 * Creates a 3d graph from an expression like f(x,y)
 * @param expression
 * @param segments
 * @returns {Mesh}
 */
export function createGraph(expression, segments = 100) {

    // true => sensible image tile repeat...
    var graphGeometry = new THREE.ParametricGeometry(ParametricExpressionFunctionFactory(expression), segments, segments, true);

    //-----------------------------

    var gradient = createGradient()
    let colorsHex = gradient.rgb(segments).map(color => '#' + color.toHex());


    let vertexColorMaterial = createGraphDefaultMaterial(graphGeometry, colorsHex)

    //-------------------------

    var graphMesh = new THREE.Mesh(graphGeometry, vertexColorMaterial);
    graphMesh.doubleSided = true;

    return graphMesh

}


/**
 * a more configurable approach
 * TODO maybe put graph generation into a builder class
 * @param expression
 * @returns {Mesh}
 */

function createGraphAlt(expression) {

    let segments = 50
    //TODO refactor and
    //alternative  more configurable approach
    let expr = new Expression("f(x,y)=" + expression)


    let graphGeometry = new THREE.PlaneGeometry(20, 20, segments, segments)
    expr.toGeometry(graphGeometry)

    //-------------------------
    var gradient = createGradient()
    let colorsHex = gradient.rgb(segments).map(color => '#' + color.toHex());


    let vertexColorMaterial = createGraphDefaultMaterial(graphGeometry, colorsHex)
    //-------------------------
    var graphMesh = new THREE.Mesh(graphGeometry, vertexColorMaterial);
    graphMesh.doubleSided = true;

    return graphMesh


}


function createGraphDefaultMaterial(graphGeometry, colorsHex) {


    var vertexColorMaterial = new THREE.MeshPhongMaterial({
        vertexColors: THREE.VertexColors,
        // color: 0xff8010,
        shading: THREE.FlatShading,
        // specular: 0xffffff,
        shininess: 1,
        ambient: 0xffffff
    });


    ///////////////////////////////////////////////
    // calculate vertex colors based on Z values //
    ///////////////////////////////////////////////
    graphGeometry.computeBoundingBox();
    var zMin = graphGeometry.boundingBox.min.z;
    var zMax = graphGeometry.boundingBox.max.z;
    var zRange = zMax - zMin;
    var color, point, face, numberOfSides, vertexIndex;
    // faces are indexed using characters
    var faceIndices = ['a', 'b', 'c', 'd'];
    // first, assign colors to vertices as desired
    for (let i = 0; i < graphGeometry.vertices.length; i++) {
        point = graphGeometry.vertices[i];
        color = new THREE.Color(0x0000ff);
        //color.setHSL( 0.7 * (yMax - point.y) / yRange, 1, 0.5 );


        //color.setHSL( , 1, 0.5 );

        let hex = colorsHex[Math.floor(colorsHex.length * (zMax - point.z) / zRange)]

        color.set(hex)


        graphGeometry.colors[i] = color; // use this array for convenience
    }
    // copy the colors as necessary to the face's vertexColors array.
    for (let i = 0; i < graphGeometry.faces.length; i++) {
        face = graphGeometry.faces[i];
        numberOfSides = (face instanceof THREE.Face3) ? 3 : 4;
        for (var j = 0; j < numberOfSides; j++) {
            vertexIndex = face[faceIndices[j]];
            face.vertexColors[j] = graphGeometry.colors[vertexIndex];
        }
    }
    ///////////////////////
    // end vertex colors //
    ///////////////////////
    return vertexColorMaterial

}