import * as _ from 'lodash'
import * as THREE from "three";

// https://github.com/tweenjs/tween.js/issues/78
// Flatten/Deflate an object


export var flatten = function (source, pathArray, result) {
    pathArray = (typeof pathArray === 'undefined') ? [] : pathArray;
    result = (typeof result === 'undefined') ? {} : result;
    var key, value, newKey;
    for (var i in source) {
        if (source.hasOwnProperty(i)) {
            key = i;
            value = source[i];
            pathArray.push(key);

            if (typeof value === 'object' && value !== null) {
                result = flatten(value, pathArray, result);
            } else {
                newKey = pathArray.join('.');
                result[newKey] = value;
            }
            pathArray.pop();
        }
    }
    return result;
};


/**
 * Returns a random point of a sphere, evenly distributed over the sphere.
 * The sphere is centered at (x0,y0,z0) with the passed in radius.
 * The returned point is returned as a three element array [x,y,z].
 * {@link https://stackoverflow.com/questions/5531827/random-point-on-a-given-sphere}
 *
 */
export
function randomSpherePoint(x0,y0,z0,radius){
    var u = Math.random();
    var v = Math.random();
    var theta = 2 * Math.PI * u;
    var phi = Math.acos(2 * v - 1);
    var x = x0 + (radius * Math.sin(phi) * Math.cos(theta));
    var y = y0 + (radius * Math.sin(phi) * Math.sin(theta));
    var z = z0 + (radius * Math.cos(phi));
    return new THREE.Vector3(x,y,z);
}

export function generateRandomSpherePoints(center=new THREE.Vector3(),radius,count=1000)
{
    let {x,y,z}=center

    return _.times(count, ()=>  randomSpherePoint(x,y,z,radius) );





}