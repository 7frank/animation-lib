import * as THREE from 'three'
import math from "mathjs";
import 'three/examples/js/modifiers/SimplifyModifier'
import {Animation} from "../animation/Animation";
import {generateRandomSpherePoints} from "./points-utils";
import * as _ from 'lodash'

/**
 * Helper that generates a point cloud of a 3d function like f(x,y)
 * @param expression
 * @returns {Points}
 * @constructor
 */
export function ExpressionPointCloudHelper(expression, step=0.1, pointSize=0.1) {


    /*
         //FIXME
       let parser = math.parser()
       parser.eval(expression)
       // e.g. parser.scope.f(1,2)

       expression="x+y"
   */

    const node = math.parse(expression)       // parse expression into a node tree
    const code = node.compile()         // compile the node tree
    // evaluate the code with an optional scope


    var starsGeometry = new THREE.Geometry();


    for (var x = -10; x < 10; x+=step) {
        for (var y = -10; y < 10; y+=step) {


            //  var val = parser.eval(`f(${x},${y})`);
            let val = code.eval({x, y})
            //console.log(x,y,val)
            var pt = new THREE.Vector3();
            pt.x = x//THREE.Math.randFloatSpread(2000);
            pt.y = y//THREE.Math.randFloatSpread(2000);
            pt.z = val//THREE.Math.randFloatSpread(2000);

            starsGeometry.vertices.push(pt);
        }
    }

    var starsMaterial = new THREE.PointsMaterial({color: 0x888888, size: pointSize});


    //var bufferGeometry = new THREE.BufferGeometry().fromGeometry( starsGeometry );

    var starField = new THREE.Points(starsGeometry, starsMaterial);

    return starField


}

/**
 * Parses and creates a helper object for evaluation.
 * @param expression
 * @returns {function(*, *): *}
 */
export
function getFunc(expression) {

    const node = math.parse(expression)       // parse expression into a node tree
    const code = node.compile()

    //var zFuncText = "x^2 - y^2";
    //var zFunc  = Parser.parse(zFuncText).toJSFunction( ['x','y'] );

    var zFunc = function (x, y) {
        return code.eval({x, y})
    }


    return zFunc
}


/**
 * Creates an animation / a morph from the source point cloud to the target point cloud
 * Note: both geometries must have same amount of vertices
 * TODO how to chain possible animations the best way
 * TODO have same morph with FBOs and shader parameter
 * @param cloudSource
 * @param cloudTarget
 * @param duration
 * @param options
 */
export function pointCloudMorph(cloudSource, cloudTarget, duration = 10000, options = {}) {


    //TODO animation is in progress but not the correct target is used  or cloned or whatever for point cloud examples

    /*
        function convert(array){
          let res={}

          array.forEach((v,k) =>  res[k]=v )

            return res
        }
    */
    let animationScript = new Animation(cloudSource.geometry.vertices, options);


   let pts= generateRandomSpherePoints(undefined,50,cloudSource.geometry.vertices.length)



    function onUpdate() {
        cloudSource.geometry.verticesNeedUpdate = true
        //cloudTarget.geometry.verticesNeedUpdate=true

    }

    function onFinish() {
        //finished

        animationScript.animate(_.random(0,1)?pts:cloudTarget.geometry.vertices, duration, onFinish, onUpdate)


    }

    animationScript.animate(cloudTarget.geometry.vertices, duration,onFinish, onUpdate)


   // animationScript.pushAnimation(pts,onUpdate).loopChain().start()




}


/**
 * morphs an array of values or objects
 */
export class Morph
{


}



