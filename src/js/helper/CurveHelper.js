import * as THREE from "three";
import math from "mathjs";
import {getFunc} from "./ExpressionPointCloudHelper";




/**
 * Helper that renders a line for a math
 * @param curve
 * @returns {Line}
 */
export function curve2line(curve, count = 100) {
    var points = curve.getPoints(count);
    var geometry = new THREE.BufferGeometry().setFromPoints(points);

    var material = new THREE.LineBasicMaterial({color: 0xff0000});

// Create the final object to add to the scene
    var splineObject = new THREE.Line(geometry, material);

    return splineObject
}

/**
 * Helper that created a line depending on a math for x,y values and a z value of an expression like f(x,y)
 * @param curve
 * @param expression
 * @returns {Line}
 */
export function curve2line2(curve, expression, count = 100) {
    let f = getFunc(expression)


    var points = curve.getPoints(count);

    let pointsProjected = points.map(pt => new THREE.Vector3(pt.x, pt.y, f(pt.x, pt.y)))

    var geometry = new THREE.BufferGeometry().setFromPoints(pointsProjected);

    var material = new THREE.LineBasicMaterial({color: 0xff0000});

// Create the final object to add to the scene
    var splineObject = new THREE.Line(geometry, material);

    return splineObject
}