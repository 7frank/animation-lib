import * as THREE from 'three';
import {FBO} from './fbo'
import {FPSCtrl} from "../animation/FPSCtrl";

import simulation_vs from '../fbo/glsl/morph/simulation_vs.glsl'
import simulation_fs from '../fbo/glsl/morph/simulation_fs.glsl'

import noise_simulation_fs from '../fbo/glsl/noise/simulation_fs.glsl'


import render_vs from '../fbo/glsl/morph/render_vs.glsl'
import render_fs from '../fbo/glsl/morph/render_fs.glsl'
import * as _ from "lodash";
import {getErrorDataTexture} from "./texture-utils";

/**
 *
 * @param {THREE.Renderer} renderer
 * @param textureOptions - texture parameters
 * @param {string|THREE.DataTexture} textureOptions.source
 * @param {string|THREE.DataTexture} textureOptions.target
 * @param {string|THREE.DataTexture} textureOptions.sourceColor
 * @param {string|THREE.DataTexture} textureOptions.targetColor
 * @param {string|THREE.DataTexture} textureOptions.particle
 *
 * @param shaderOptions - shader specific options
 * @param {number} shaderOptions.pointSize
 * @param {boolean} shaderOptions.sizeAttenuation
 *
 * @param miscOptions - other options.
 * @param miscOptions.width
 * @param miscOptions.height
 * @returns {{fbo: FBO, simulationShader: ShaderMaterial, renderShader: ShaderMaterial}}
 */
export function createFBOMorph(renderer, textureOptions, shaderOptions = {}, miscOptions) {

    //to be able to load and render remote files
    THREE.ImageUtils.crossOrigin = '';
    //wrap strings into texture objects
    _.each(textureOptions, (v, k) => textureOptions[k] = typeof v == "string" ? THREE.ImageUtils.loadTexture(v) : v)

    //texture objects
    let {source, target, sourceColor, targetColor, particle} = textureOptions

    if (!source) throw new Error("missing data texture: source")
    if (!target) throw new Error("missing data texture: target")
  //  if (!sourceColor) throw new Error("missing data texture: sourceColor")



    miscOptions = _.assignIn({width: 256, height: 256}, miscOptions)

    if (!sourceColor)
        sourceColor = getErrorDataTexture("Missing sourceColor.",miscOptions.width,miscOptions.height)



    shaderOptions = _.assignIn({pointSize: 1, sizeAttenuation: true}, shaderOptions)


    var simulationShader = new THREE.ShaderMaterial({

        uniforms: {
            textureA: {type: "t", value: source},
            textureB: {type: "t", value: target},
            timer: {type: "f", value: 0}
        },
        vertexShader: simulation_vs,
        fragmentShader: simulation_fs

    });


    let renderShaderUniforms = {
        positions: {type: "t", value: null},
        pointSize: {type: "f", value: shaderOptions.pointSize},
        sizeAttenuation: {type: "b", value: shaderOptions.sizeAttenuation},

        alpha: {type: "f", value: .5},
        tParticle: {type: "t", value: particle},
        tColors: {type: "t", value: sourceColor}

    }


    let renderShaderOptions = {
        uniforms: renderShaderUniforms,
        vertexShader: render_vs,
        fragmentShader: render_fs,
        transparent: false,
        blending: THREE.AdditiveBlending,
        depthTest: false  // disable depth test when using particle texture
    }

    renderShaderOptions = _.assignIn(renderShaderOptions, shaderOptions)


    var renderShader = new THREE.ShaderMaterial(renderShaderOptions);
    var fbo = new FBO()

    fbo.init(miscOptions.width, miscOptions.height, renderer, simulationShader, renderShader);


    new FPSCtrl(60, function (info) {

        // let t = info.frame % 400 / 400
        // t = (--t) * t * t + 1
        //update params
        //simulationShader.uniforms.timer.value = t;
        // fbo.particles.rotation.y -= Math.PI / 180 * .1;
        //renderShader.uniforms.pointSize.value = 5
        //update simulation
        fbo.update();


    }, this).start()



    return {fbo, simulationShader, renderShader}

}



export function createFBONoise(renderer, textureOptions, shaderOptions = {}, miscOptions) {

    //to be able to load and render remote files
    THREE.ImageUtils.crossOrigin = '';
    //wrap strings into texture objects
    _.each(textureOptions, (v, k) => textureOptions[k] = typeof v == "string" ? THREE.ImageUtils.loadTexture(v) : v)

    //texture objects
    let {source, target, sourceColor, targetColor, particle} = textureOptions

    if (!source) throw new Error("missing data texture: source")
   // if (!target) throw new Error("missing data texture: target")
   // if (!sourceColor) throw new Error("missing data texture: sourceColor")



    miscOptions = _.assignIn({width: 256, height: 256}, miscOptions)


    if (!sourceColor)
        sourceColor = getErrorDataTexture("Missing sourceColor.",miscOptions.width,miscOptions.height)



    shaderOptions = _.assignIn({pointSize: 1, sizeAttenuation: true}, shaderOptions)


    var simulationShader = new THREE.ShaderMaterial({

        uniforms: {
            texture: {type: "t", value: source},
          //  textureB: {type: "t", value: target},
            timer: {type: "f", value: 0},


          //noise related
            frequency: { type: "f", value: 0.01 },
            amplitude: { type: "f", value: 96 },
            maxDistance: { type: "f", value: 48 }


        },
        vertexShader: simulation_vs,
        fragmentShader: noise_simulation_fs

    });


    let renderShaderUniforms = {
        positions: {type: "t", value: null},
        pointSize: {type: "f", value: shaderOptions.pointSize},
        sizeAttenuation: {type: "b", value: shaderOptions.sizeAttenuation},

        alpha: {type: "f", value: .5},
        tParticle: {type: "t", value: particle},
        tColors: {type: "t", value: sourceColor}



    }


    let renderShaderOptions = {
        uniforms: renderShaderUniforms,
        vertexShader: render_vs,
        fragmentShader: render_fs,
        transparent: false,
        blending: THREE.AdditiveBlending,
        depthTest: false  // disable depth test when using particle texture
    }

    renderShaderOptions = _.assignIn(renderShaderOptions, shaderOptions)


    var renderShader = new THREE.ShaderMaterial(renderShaderOptions);
    var fbo = new FBO()

    fbo.init(miscOptions.width, miscOptions.height, renderer, simulationShader, renderShader);


    new FPSCtrl(60, function (info) {

        //noise specific
        simulationShader.uniforms.timer.value += 0.01;
        fbo.particles.rotation.x = Math.cos( Date.now() *.001 ) * Math.PI / 180 * 2;
        fbo.particles.rotation.y -= Math.PI / 180 * .05;
        // let t = info.frame % 400 / 400
        // t = (--t) * t * t + 1
        //update params
        //simulationShader.uniforms.timer.value = t;
        // fbo.particles.rotation.y -= Math.PI / 180 * .1;
        //renderShader.uniforms.pointSize.value = 5
        //update simulation
        fbo.update();


    }, this).start()



    return {fbo, simulationShader, renderShader}

}