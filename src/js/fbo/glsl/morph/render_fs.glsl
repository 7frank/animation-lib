uniform sampler2D tParticle;
uniform sampler2D tColors;

varying vec2 texcoord;

void main()
{



// (0) draw raw points
// gl_FragColor = vec4( vec3( 1. ), .25 );

// (1) draw particles from texture only without color changes
//gl_FragColor = texture2D(tParticle, gl_PointCoord);


// (2) draw texture colors without particles from texture
//gl_FragColor = texture2D(tColors, texcoord);


/*
// mix approaches (1) + (2)
vec4 particleColor = texture2D(tParticle, gl_PointCoord);
vec4 ptColor = texture2D(tColors, texcoord);
gl_FragColor = mix(ptColor,particleColor,0.5);
*/

//use only alpha channel of particle
vec4 particleColor = texture2D(tParticle, gl_PointCoord);
vec4 ptColor = texture2D(tColors, texcoord);


gl_FragColor =vec4( vec3(ptColor.xyz ), particleColor.a );




}