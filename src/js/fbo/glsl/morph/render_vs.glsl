
//float texture containing the positions of each particle
uniform sampler2D positions;

//size
uniform float pointSize;

//used for size attenuation
uniform bool sizeAttenuation;

//used for color mappings in fs
varying vec2 texcoord;



void main() {


    //the mesh is a normalized square so the uvs = the xy positions of the vertices
    vec3 pos = texture2D( positions, position.xy ).xyz;

    texcoord  = position.xy;// uv;

    //pos now contains the position of a point in space taht can be transformed
    gl_Position = projectionMatrix * modelViewMatrix * vec4( pos, 1.0 );

    //TODO  check out better size attenuation like in {@link https://gamedev.stackexchange.com/questions/54391/scaling-point-sprites-with-distance}
    if (sizeAttenuation)
     gl_PointSize = pointSize/gl_Position.w;
     else
     gl_PointSize = pointSize;


}