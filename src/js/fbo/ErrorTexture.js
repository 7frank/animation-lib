import * as THREE from "three";


/**
 * base class for canvas based texture generation
 */

export class CanvasTexture
{
    constructor(width=512,height=512) {

        this.width=width
        this.height=height

    this.createCanvas()


    }


    createCanvas() {

        this.canvas = document.createElement('canvas')
        this.canvas.width=this.width
        this.canvas.height=this.height

        this.ctx = this.canvas.getContext('2d')
        return this

    }

    getCanvas() {
        return this.canvas
    }

    getDataURL() {
        return this.canvas.toDataURL()
    }


    getDataTexture() {
        let canvas = this.canvas

        var canvasData = canvas.getContext('2d').getImageData(0, 0, canvas.width, canvas.height);


        var data = new Uint8Array(canvasData.data);
        let texture = new THREE.DataTexture(data, canvas.width, canvas.height, THREE.RGBAFormat);
        texture.needsUpdate = true;

        return texture
    }







    /**
     * TODO convert the texture into positional information
     */
    getVectorDataTexture() {
        throw new Error("missing implementation")
    }

    getTexture() {
        return new THREE.Texture(this.canvas);


    }




}


export class ErrorTexture extends CanvasTexture{



    setErrorMessage(text, width, height) {

        let ctx = this.ctx
        let canvas = this.canvas
        ctx.font = '20pt Arial';
        ctx.fillStyle = 'red';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = 'yellow';
        ctx.fillRect(10, 10, canvas.width - 20, canvas.height - 20);
        ctx.fillStyle = 'red';
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(text, canvas.width / 2, canvas.height / 2);

        // TODO this might inverse the image when placed at the right position
        // ctx.scale(-1, 1);

        return this
    }



}

