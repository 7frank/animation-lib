import * as THREE from "three";
import {ErrorTexture} from "./ErrorTexture";


function getPoint(v, size) {
    v.x = Math.random() * 2 - 1;
    v.y = Math.random() * 2 - 1;
    v.z = Math.random() * 2 - 1;
    if (v.length() > 1) return getPoint(v, size);
    return v.normalize().multiplyScalar(size);
}

function getRandomData(center, width, height, size) {


    if (!center) center = new THREE.Vector3()

    var len = width * height * 3;
    var data = new Float32Array(len);
    while (len >= 0) {
        len -= 3
        data[len] = (Math.random() - .5) * size + center.x;
        data[len + 1] = (Math.random() - .5) * size + center.y;
        data[len + 2] = (Math.random() - .5) * size + center.z;
    }
    return data;
}

function getSphere(count, size, useZ = true) {

    var len = count * 3;
    var data = new Float32Array(len);
    var p = new THREE.Vector3();
    for (var i = 0; i < len; i += 3) {
        getPoint(p, size);
        data[i] = p.x;
        data[i + 1] = p.y;
        data[i + 2] = useZ ? p.z : 0;
    }
    return data;
}

/**
 * returns a Float32Array buffer of spherical 3D points
 */



export function getSphereDataTexture(width, height, size = 100,useZ=true) {
    //get some random points on large sphere
    var dataB = getSphere(width * height, size, useZ);
    var sphereTexture = new THREE.DataTexture(dataB, width, height, THREE.RGBFormat, THREE.FloatType, THREE.DEFAULT_MAPPING, THREE.RepeatWrapping, THREE.RepeatWrapping);
    sphereTexture.needsUpdate = true;
    return sphereTexture
}

/**
 * returns a Float32Array buffer of random 3D coordinates
 * @param {THREE.Vector3} center
 * @param {number} width - Width of the texture.
 * @param {number} height - Height of the texture.
 * @param {number} size - e.g. the radius
 * @returns {DataTexture}
 */
export function getRandomDataTexture(center, width, height, size) {


    var dataA = getRandomData(center, width, height, size);
    var textureA = new THREE.DataTexture(dataA, width, height, THREE.RGBFormat, THREE.FloatType, THREE.DEFAULT_MAPPING, THREE.RepeatWrapping, THREE.RepeatWrapping);
    textureA.needsUpdate = true;
    return textureA
}



/**
 *
 * @deprecated
 * @returns {DataTexture}
 */
export function getErrorDataTexture(errorMessage, width, height) {

/*
    var size = width * height;
    var data = new Uint8Array(3 * size);

    let color = new THREE.Color("red")

    var r = Math.floor(color.r * 255);
    var g = Math.floor(color.g * 255);
    var b = Math.floor(color.b * 255);

    for (var i = 0; i < size; i++) {

        var stride = i * 3;

        data[stride] = r;
        data[stride + 1] = g;
        data[stride + 2] = b;

    }

// used the buffer to create a DataTexture

    var texture = new THREE.DataTexture(data, width, height, THREE.RGBFormat);
    texture.needsUpdate = true


return texture
*/


return new ErrorTexture().setErrorMessage(errorMessage, width, height).getDataTexture()


}
