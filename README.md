npm install

npm start

npm build

## To Use Orbit Controls

`npm i orbit-controls-es6 --save`

in `main.js`:

```
import OrbitControls from 'orbit-controls-es6'
 
this.controls = new OrbitControls(this.camera, this.renderer.domElement)
this.controls.enabled = true
this.controls.maxDistance = 1500
this.controls.minDistance = 0
```
## TODO
* check out ideas for animation lib
    * http://mathis-biabiany.fr/experiment
* TODO spline easing function 
    * http://greweb.me/2012/02/bezier-curve-based-easing-functions-from-concept-to-implementation/
    * alternatively create spline easing class
* TODO create a simple image gallery for demo purposes    
* build CI step and link to demo code
* use typescript    